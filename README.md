# Yayaka Reference

## Environment Variables

- *HOST*  
  The hostname
- *DATABASE_URL*
- *POOL_SIZE*
- *GUARDIAN_SECRET_KEY*  
  A secret key to sign the api tokens.
- *SECRET_KEY_BASE*  
  A secret key to sign cookies, tokens, etc.
